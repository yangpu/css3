







# css3

## 2D 转换

- translate()
- rotate()
- scale()
- skew()
- matrix()



### translate() 方法

translate()方法，根据左(X轴)和顶部(Y轴)位置给定的参数，从当前元素位置移动。

translate值（50px，100px）是从左边元素移动50个像素，并从顶部移动100像素。

```
div
{
transform: translate(50px,100px);
-ms-transform: translate(50px,100px); /* IE 9 */
-webkit-transform: translate(50px,100px); /* Safari and Chrome */
}
```



### rotate() 方法

rotate()方法，在一个给定度数顺时针旋转的元素。负值是允许的，这样是元素逆时针旋转。

rotate值（30deg）元素顺时针旋转30度

```
div
{
transform: rotate(30deg);
-ms-transform: rotate(30deg); /* IE 9 */
-webkit-transform: rotate(30deg); /* Safari and Chrome */
}
```



### scale() 方法

scale()方法，该元素增加或减少的大小，取决于宽度（X轴）和高度（Y轴）的参数：

scale（2,3）转变宽度为原来的大小的2倍，和其原始大小3倍的高度。

```
-ms-transform:scale(2,3) translate(20px); /* IE 9 */
-webkit-transform: scale(2,3); /* Safari */
transform: scale(2,3); /* 标准语法 */
```



### skew() 方法

包含两个参数值，分别表示X轴和Y轴倾斜的角度，如果第二个参数为空，则默认为0，参数为负表示向相反方向倾斜。

- skewX(<angle>);表示只在X轴(水平方向)倾斜。
- skewY(<angle>);表示只在Y轴(垂直方向)倾斜。

skew(30deg,20deg) 元素在X轴和Y轴上倾斜20度30度。

```
div
{
transform: skew(30deg,20deg);
-ms-transform: skew(30deg,20deg); /* IE 9 */
-webkit-transform: skew(30deg,20deg); /* Safari and Chrome */
}
```

skew（0，30deg）；是按照水平方向Y轴，顺时针旋转

通过skew（）函数将长方形变成平行四边形。



### matrix() 方法

matrix()方法和2D变换方法合并成一个。

matrix 方法有六个参数，包含旋转，缩放，移动（平移）和倾斜功能

```
div
{
transform:matrix(0.866,0.5,-0.5,0.866,0,0);
-ms-transform:matrix(0.866,0.5,-0.5,0.866,0,0); /* IE 9 */
-webkit-transform:matrix(0.866,0.5,-0.5,0.866,0,0); /* Safari and Chrome */
}
```



| 函数                            | 描述                                     |
| ------------------------------- | ---------------------------------------- |
| matrix(*n*,*n*,*n*,*n*,*n*,*n*) | 定义 2D 转换，使用六个值的矩阵。         |
| translate(*x*,*y*)              | 定义 2D 转换，沿着 X 和 Y 轴移动元素。   |
| translateX(*n*)                 | 定义 2D 转换，沿着 X 轴移动元素。        |
| translateY(*n*)                 | 定义 2D 转换，沿着 Y 轴移动元素。        |
| scale(*x*,*y*)                  | 定义 2D 缩放转换，改变元素的宽度和高度。 |
| scaleX(*n*)                     | 定义 2D 缩放转换，改变元素的宽度。       |
| scaleY(*n*)                     | 定义 2D 缩放转换，改变元素的高度。       |
| rotate(*angle*)                 | 定义 2D 旋转，在参数中规定角度。         |
| skew(*x-angle*,*y-angle*)       | 定义 2D 倾斜转换，沿着 X 和 Y 轴。       |
| skewX(*angle*)                  | 定义 2D 倾斜转换，沿着 X 轴。            |
| skewY(*angle*)                  | 定义 2D 倾斜转换，沿着 Y 轴。            |



## 3D 转换

### rotateX() 方法

rotateX()方法，围绕其在一个给定度数X轴旋转的元素。

```
div
{
    transform: rotateX(120deg);
    -webkit-transform: rotateX(120deg); /* Safari 与 Chrome */
}
```



### rotateY() 方法

rotateY()方法，围绕其在一个给定度数Y轴旋转的元素。



### 3D 转换方法

| 函数                                                         | 描述                                      |
| ------------------------------------------------------------ | ----------------------------------------- |
| matrix3d(*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*,*n*) | 定义 3D 转换，使用 16 个值的 4x4 矩阵。   |
| translate3d(*x*,*y*,*z*)                                     | 定义 3D 转化。                            |
| translateX(*x*)                                              | 定义 3D 转化，仅使用用于 X 轴的值。       |
| translateY(*y*)                                              | 定义 3D 转化，仅使用用于 Y 轴的值。       |
| translateZ(*z*)                                              | 定义 3D 转化，仅使用用于 Z 轴的值。       |
| scale3d(*x*,*y*,*z*)                                         | 定义 3D 缩放转换。                        |
| scaleX(*x*)                                                  | 定义 3D 缩放转换，通过给定一个 X 轴的值。 |
| scaleY(*y*)                                                  | 定义 3D 缩放转换，通过给定一个 Y 轴的值。 |
| scaleZ(*z*)                                                  | 定义 3D 缩放转换，通过给定一个 Z 轴的值。 |
| rotate3d(*x*,*y*,*z*,*angle*)                                | 定义 3D 旋转。                            |
| rotateX(*angle*)                                             | 定义沿 X 轴的 3D 旋转。                   |
| rotateY(*angle*)                                             | 定义沿 Y 轴的 3D 旋转。                   |
| rotateZ(*angle*) = ratate()                                  | 定义沿 Z 轴的 3D 旋转。                   |
| perspective(*n*)                                             | 定义 3D 转换元素的透视视图。              |

### 转换属性

| 属性                | 描述                                 | CSS  |
| ------------------- | ------------------------------------ | ---- |
| transform           | 向元素应用 2D 或 3D 转换。           | 3    |
| transform-origin    | 允许你改变被转换元素的位置。         | 3    |
| transform-style     | 规定被嵌套元素如何在 3D 空间中显示。 | 3    |
| perspective         | 规定 3D 元素的透视效果。             | 3    |
| perspective-origin  | 规定 3D 元素的底部位置。             | 3    |
| backface-visibility | 定义元素在不面对屏幕时是否可见。     | 3    |

### transform 属性

> 应用于元素的2D或3D转换。这个属性允许你将元素旋转，缩放，移动，倾斜等

`transform: none|transform-functions;`



### transform-origin 属性

> transform-Origin属性允许您更改转换元素的位置。改变中心点，主要用于ratate()
>
> **注意：** 使用此属性必须先使用 transform属性.
>
> 2D转换元素可以改变元素的X和Y轴。 3D转换元素，还可以更改元素的Z轴

`transform-origin: x-axis y-axis z-axis;`

| 值     | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| x-axis | 定义视图被置于 X 轴的何处。可能的值：left   center   right    length    % |
| y-axis | 定义视图被置于 Y 轴的何处。可能的值：top  center  bottom   length  % |
| z-axis | 定义视图被置于 Z 轴的何处。可能的值：*length*                |

### transform-style 属性

> transform--style属性指定**嵌套元素**是怎样在三维空间中呈现。父元素
>
> **注意：** 使用此属性必须先使用 transform属性.

`transform-style: flat|preserve-3d;`

| 值          | 描述                           |
| ----------- | ------------------------------ |
| flat        | 表示所有子元素在2D平面呈现。   |
| preserve-3d | 表示所有子元素在3D空间中呈现。 |

### perspective 属性

> 多少像素的3D元素是从视图的perspective属性定义。父元素
>
> 这个属性允许你改变3D元素是怎样查看透视图。
>
> 定义时的perspective属性，它是一个元素的子元素，透视图，而**不是元素本身**。
>
> **注意：**perspective 属性只影响 3D 转换元素。
>
> **提示:** 请与 perspective-origin 属性一同使用该属性，这样您就能够改变 3D 元素的底部位置。

`perspective: number|none`

| 值       | 描述                            |
| -------- | ------------------------------- |
| *number* | 元素距离视图的距离，以像素计。  |
| none     | 默认值。与 0 相同。不设置透视。 |



### perspective-origin 属性

> perspective-origin 属性定义 3D 元素所基于的 X 轴和 Y 轴。该属性允许您改变 3D 元素的底部位置。父元素
>
> 定义时的perspective -Origin属性，它是一个元素的子元素，透视图，而不是元素本身。

`perspective-origin: x-axis y-axis;`

| 值       | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| *x-axis* | 定义该视图在 x 轴上的位置。默认值：50%。可能的值：left center right  length   % |
| *y-axis* | 定义该视图在 y 轴上的位置。默认值：50%。可能的值：top center bottom  length % |

### backface-visibility 属性

> backface-visibility 属性定义当元素不面向屏幕时是否可见。
>
> 如果在旋转元素不希望看到其背面时，该属性很有用

| 值      | 描述             |
| ------- | ---------------- |
| visible | 背面是可见的。   |
| hidden  | 背面是不可见的。 |

## CSS3 过渡

> CSS3 过渡是元素从一种样式逐渐改变为另一种的效果

- 指定要添加效果的CSS属性
- 指定效果的持续时间。

应用于宽度属性的过渡效果，时长为 2 秒：

 如果未指定的期限，transition将没有任何效果，因为默认值是0

```
div
{
    transition: width 2s;
    -webkit-transition: width 2s; /* Safari */
}
```



### 多项改变

要添加多个样式的变换效果，添加的属性由逗号分隔

添加了宽度，高度和转换效果

```
div
{
    transition: width 2s, height 2s, transform 2s;
    -webkit-transition: width 2s, height 2s, -webkit-transform 2s;
}
```



### 过渡属性

| 属性                       | 描述                                         | CSS                                        |
| -------------------------- | -------------------------------------------- | ------------------------------------------ |
| transition                 | 简写属性，用于在一个属性中设置四个过渡属性。 | 3                                          |
| transition-property        | 规定应用过渡的 CSS 属性的名称。              | 指定CSS属性的name，transition效果          |
| transition-duration        | 定义过渡效果花费的时间。默认是 0。           | transition效果需要指定多少秒或毫秒才能完成 |
| transition-timing-function | 规定过渡效果的时间曲线。默认是 "ease"。      | 指定transition效果的转速曲线               |
| transition-delay           | 规定过渡效果何时开始。默认是 0。             | 定义transition效果开始的时候               |

### transition 属性

> transition属性是一个速记属性有四个属性：transition-property, transition-duration, transition-timing-function, and transition-delay。

`transition: property duration timing-function delay;`

**注意：** 始终指定transition-duration属性，否则持续时间为0，transition不会有任何效果。

| 默认值：          | all 0 ease 0                         |
| ----------------- | ------------------------------------ |
| 继承：            | no                                   |
| 版本：            | CSS3                                 |
| JavaScript 语法： | *object*.style.transition="width 2s" |



#### transition-property属性

> transition-property属性指定CSS属性的nametransition效果（transition效果时将会启动指定的CSS属性的变化）。
>
> **提示:**一个转场效果，通常会出现在当用户将鼠标悬停在一个元素上时。

`transition-property: none|all| property;`

| 值         | 描述                                                  |
| ---------- | ----------------------------------------------------- |
| none       | 没有属性会获得过渡效果。                              |
| all        | 所有属性都将获得过渡效果。                            |
| *property* | 定义应用过渡效果的 CSS 属性名称列表，列表以逗号分隔。 |

#### transition-duration 属性

> transition-duration 属性规定完成过渡效果需要花费的时间（以秒或毫秒计）

`transition-duration: time;`

| 值     | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| *time* | 规定完成过渡效果需要花费的时间（以秒或毫秒计）。 默认值是 0，意味着不会有效果。 |

#### transition-timing-function 属性

> transition-timing-function属性指定切换效果的速度。
>
> 此属性允许一个过渡效果，以改变其持续时间的速度。

`transition-timing-function: linear|ease|ease-in|ease-out|ease-in-out|cubic-bezier(*n*,*n*,*n*,*n*);`

| 值                            | 描述                                                         |
| ----------------------------- | ------------------------------------------------------------ |
| linear                        | 规定以相同速度开始至结束的过渡效果（等于 cubic-bezier(0,0,1,1)）。 |
| ease                          | 规定慢速开始，然后变快，然后慢速结束的过渡效果（cubic-bezier(0.25,0.1,0.25,1)）。 |
| ease-in                       | 规定以慢速开始的过渡效果（等于 cubic-bezier(0.42,0,1,1)）。  |
| ease-out                      | 规定以慢速结束的过渡效果（等于 cubic-bezier(0,0,0.58,1)）。  |
| ease-in-out                   | 规定以慢速开始和结束的过渡效果（等于 cubic-bezier(0.42,0,0.58,1)）。 |
| cubic-bezier(*n*,*n*,*n*,*n*) | 在 cubic-bezier 函数中定义自己的值。可能的值是 0 至 1 之间的数值。 |



#### transition-delay 属性

> transition-delay 属性指定何时将开始切换效果。
>
> transition-delay值是指以秒为单位（S）或毫秒（ms）。

`transition-delay: time;`指定秒或毫秒数之前要等待切换效果开始



## CSS3 动画

要创建CSS3动画，你将不得不了解@keyframes规则。

@keyframes规则是创建动画。 @keyframes规则内指定一个CSS样式和动画将逐步从目前的样式更改为新的样式。

```
@keyframes myfirst
{
    from {background: red;}
    to {background: yellow;}
}
```



当在 **@keyframes** 创建动画，把它绑定到一个选择器，否则动画不会有任何效果。

指定至少这两个CSS3的动画属性绑定向一个选择器：

- 规定动画的名称
- 规定动画的时长

您必须定义动画的名称和动画的持续时间。如果省略的持续时间，动画将无法运行，因为默认值是0。

```
div
{
    animation: myfirst 5s;
}
```

### CSS3动画是什么

> 动画是使元素从一种样式逐渐变化为另一种样式的效果。
>
> 您可以改变任意多的样式任意多的次数。
>
> 请用百分比来规定变化发生的时间，或用关键词 "from" 和 "to"，等同于 0% 和 100%。
>
> 0% 是动画的开始，100% 是动画的完成。
>
> 为了得到最佳的浏览器支持，您应该始终定义 0% 和 100% 选择器

```
@keyframes myfirst
{
    0%   {background: red;}
    25%  {background: yellow;}
    50%  {background: blue;}
    100% {background: green;}
}
```



### CSS3的动画属性

| 属性                                                         | 描述                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [@keyframes](https://www.runoob.com/cssref/css3-pr-animation-keyframes.html) | 规定动画。                                                   |
| [animation](https://www.runoob.com/cssref/css3-pr-animation.html) | 所有动画属性的简写属性，除了 animation-play-state 属性。     |
| [animation-name](https://www.runoob.com/cssref/css3-pr-animation-name.html) | 规定 @keyframes 动画的名称。                                 |
| [animation-duration](https://www.runoob.com/cssref/css3-pr-animation-duration.html) | 规定动画完成一个周期所花费的秒或毫秒。默认是 0。             |
| [animation-timing-function](https://www.runoob.com/cssref/css3-pr-animation-timing-function.html) | 规定动画的速度曲线。默认是 "ease"。                          |
| [animation-fill-mode](https://www.runoob.com/cssref/css3-pr-animation-fill-mode.html) | 规定当动画不播放时（当动画完成时，或当动画有一个延迟未开始播放时），要应用到元素的样式。 |
| [animation-delay](https://www.runoob.com/cssref/css3-pr-animation-delay.html) | 规定动画何时开始。默认是 0。                                 |
| [animation-iteration-count](https://www.runoob.com/cssref/css3-pr-animation-iteration-count.html) | 规定动画被播放的次数。默认是 1。                             |
| [animation-direction](https://www.runoob.com/cssref/css3-pr-animation-direction.html) | 规定动画是否在下一周期逆向地播放。默认是 "normal"。          |

### @keyframes 规则

`@keyframes animationname {keyframes-selector {css-styles;}}`



| 值                   | 说明                                                         |
| -------------------- | ------------------------------------------------------------ |
| *animationname*      | 必需的。定义animation的名称。                                |
| *keyframes-selector* | 必需的。动画持续时间的百分比。合法值：0-100%from (和0%相同)to (和100%相同)**注意：** 您可以用一个动画keyframes-selectors。 |
| *css-styles*         | 必需的。一个或多个合法的CSS样式属性                          |

### animation（动画） 属性

`animation: name duration timing-function delay iteration-count direction fill-mode play-state;`

默认`none 0 ease 0 1 normal`

```
div
{
    animation:mymove 5s infinite;
    -webkit-animation:mymove 5s infinite; /* Safari 和 Chrome */
}
```



#### animation-name 属性

`animation-name: keyframename|none;`

| 值             | 说明                                     |
| -------------- | ---------------------------------------- |
| *keyframename* | 指定要绑定到选择器的关键帧的名称         |
| none           | 指定有没有动画（可用于覆盖从级联的动画） |

#### animation-duration 属性

animation-duration属性定义动画完成一个周期需要多少秒或毫秒

`animation-duration: time;`

| 值     | 说明                                                         |
| ------ | ------------------------------------------------------------ |
| *time* | 指定动画播放完成花费的时间。默认值为 0，意味着没有动画效果。 |

#### animation-timing-function 属性

animation-timing-function指定动画将如何完成一个周期。

速度曲线定义动画从一套 CSS 样式变为另一套所用的时间。

`animation-timing-function: value;`

animation-timing-function使用的数学函数，称为三次贝塞尔曲线，速度曲线。使用此函数，您可以使用您自己的值，或使用预先定义的值之一：

| 值                            | 描述                                                         |
| ----------------------------- | ------------------------------------------------------------ |
| linear                        | 动画从头到尾的速度是相同的。                                 |
| ease                          | 默认。动画以低速开始，然后加快，在结束前变慢。               |
| ease-in                       | 动画以低速开始。                                             |
| ease-out                      | 动画以低速结束。                                             |
| ease-in-out                   | 动画以低速开始和结束。                                       |
| cubic-bezier(*n*,*n*,*n*,*n*) | 在 cubic-bezier 函数中自己的值。可能的值是从 0 到 1 的数值。 |

#### animation-delay 属性

animation-delay 属性定义动画什么时候开始。

animation-delay 值单位可以是秒（s）或毫秒（ms）。

**提示:** 允许负值，-2s 使动画马上开始，但跳过 2 秒进入动画。

`animation-delay: time;`

| 值     | 描述                                                    |
| ------ | ------------------------------------------------------- |
| *time* | 可选。定义动画开始前等待的时间，以秒或毫秒计。默认值为0 |

#### animation-iteration-count 属性

animation-iteration-count属性定义动画应该播放多少次

`animation-iteration-count: value;`

| 值       | 描述                             |
| -------- | -------------------------------- |
| *n*      | 一个数字，定义应该播放多少次动画 |
| infinite | 指定动画应该播放无限次（永远）   |

#### animation-direction 属性

animation-direction 属性定义是否循环交替反向播放动画。

**注意：**如果动画被设置为只播放一次，该属性将不起作用。

| 值                | 描述                                                         |
| ----------------- | ------------------------------------------------------------ |
| normal            | 默认值。动画按正常播放。                                     |
| reverse           | 动画反向播放。                                               |
| alternate         | 动画在奇数次（1、3、5...）正向播放，在偶数次（2、4、6...）反向播放。 |
| alternate-reverse | 动画在奇数次（1、3、5...）反向播放，在偶数次（2、4、6...）正向播放。 |
| initial           | 设置该属性为它的默认值。请参阅 [*initial*](https://www.runoob.com/cssref/css-initial.html)。 |
| inherit           | 从父元素继承该属性。请参阅 [*inherit*](https://www.runoob.com/cssref/css-inherit.html)。 |



#### animation-fill-mode 属性

> animation-fill-mode 属性规定当动画不播放时（当动画完成时，或当动画有一个延迟未开始播放时），要应用到元素的样式。
>
> 默认情况下，CSS 动画在第一个关键帧播放完之前不会影响元素，在最后一个关键帧完成后停止影响元素。animation-fill-mode 属性可重写该行为。

`animation-fill-mode: none|forwards|backwards|both|initial|inherit;`

| 值        | 描述                                                         |
| --------- | ------------------------------------------------------------ |
| none      | 默认值。动画在动画执行之前和之后不会应用任何样式到目标元素。 |
| forwards  | 在动画结束后（由 animation-iteration-count 决定），动画将应用该属性值。**即使动画结束后就停在那里** |
| backwards | 动画将应用在 animation-delay 定义期间启动动画的第一次迭代的关键帧中定义的属性值。这些都是 from 关键帧中的值（当 animation-direction 为 "normal" 或 "alternate" 时）或 to 关键帧中的值（当 animation-direction 为 "reverse" 或 "alternate-reverse" 时）。 |
| both      | 动画遵循 forwards 和 backwards 的规则。也就是说，动画会在两个方向上扩展动画属性。 |
| initial   | 设置该属性为它的默认值。请参阅 [*initial*](https://www.runoob.com/cssref/css-initial.html)。 |
| inherit   | 从父元素继承该属性。请参阅 [*inherit*](https://www.runoob.com/cssref/css-inherit.html)。 |



#### animation-play-state 属性

> animation--play-state属性指定动画是否正在运行或已暂停。
>
> **注意：**在JavaScript中使用此属性在一个周期中暂停动画。**可以随时暂停动画的位置**
>
> 默认值:	running

`animation-play-state: paused|running;`

| 值      | 描述               |
| ------- | ------------------ |
| paused  | 指定暂停动画       |
| running | 指定正在运行的动画 |

## 参考文档

- [CSS参考手册](http://www.css88.com/book/css/)
- [CSS3 教程](https://www.runoob.com/css3/css3-tutorial.html)
- [CSS 参考手册](https://www.runoob.com/cssref/css-functions.html)

